# Quick Start

## Clone the repo

```bash
git clone git@gitlab.com:Asacolips/gatsby-ddev.git
cd gatsby-ddev
```

## Start the project

```bash
ddev start
ddev . drush si standard --account-name=gatsby --account-pass=gatsby --site-name="Gatsby"
ddev . drush en jsonapi -y
ddev . drush cr
```

## Add a few **Article** nodes.

Gatsby will need a few article nodes to show content. Login with drush and create a few article nodes.

```bash
ddev . drush uli
```

## Confirm gatsby is running.

The gatsby container can take a short while to start since it also needs to run `npm install` prior to running `gatsby develop`. You can view progress by outputting the current logs for the gatsby container:

```bash
ddev logs -f -s gatsby
```

## Rebuild the gatsby project.

To rerun the build manually, you can run `gatsby build` within the gatsby container. To run commands from within the gatsby container, you can use `ddev . -s gatsby` to specify the gatsby container service. The full command would be:

```bash
ddev . -s gatsby -- gatsby build
```

## Accessing the backend and frontend.

The Drupal site can be accessed at http://decoupled-project.ddev.site.

The Gatsby site can be accessed at http://gatsby.decoupled-project.ddev.site, and the GraphQL explorer can be found at http://gatsby.decoupled-project.ddev.site/__graphql

# Project Structure

This project assumes a standard composer-based project structure for Drupal 8, with `composer.json` being placed at the top level, and the webroot being in the `/web` dir. The Gatsby front-end is placed in `/gatsby`, and includes an `articles.js` to demo how to query and render article nodes.

Because the containers in this DDEV project are aware of each other, the main container that Drupal runs from can be accessed by the Gatsby container at `http://web`. For example, Gatsby's `gatsby-config.js` includes this for its Drupal source config:

```js
{
  resolve: `gatsby-source-drupal`,
  options: {
    baseUrl: `http://web`,
  },
},
```

The files used to start and control the Gatsby container are located in the `.ddev` directory.

* `.ddev/gatsby-config/gatsby.Dockerfile`: This Dockerfile installs NPM and the Gatsby CLI as the container's base image.
* `.ddev/docker-compose.gatsby.yaml`: This docker-compose file defines the container service for Gatsby. On startup, it installs NPM dependencies and runs the Gatsby develop server. Port 80 on your host machine is mapped to port 8000 for Gatsby, and the URL is specified as `gatsby.${DDEV_HOSTNAME}` so that it could work for any project name as a new subdomain. Gatsby's files are pulled from a volume mapped to the `/gatsby` dir in the root of this repo, which could either be included in the repo itself for your project, or could be handled by some other means such as a git submodule.