import React from "react"
import { graphql } from "gatsby"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

class IndexPage extends React.Component {
  render() {
    const pages = this.props.data.allNodeArticle.edges
    const pageTitles = pages.map(page => (
      <li>
        <h2>{page.node.title}</h2>
        <section
          dangerouslySetInnerHTML={{ __html: page.node.body.value }}
        ></section>
      </li>
    ))
    return (
      <Layout>
        <SEO title="Home" />
        <h1>Articles</h1>
        <ul>{pageTitles}</ul>
        <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
          <Image />
        </div>
        <Link to="/">Go to Home</Link>
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query pageQuery {
    allNodeArticle {
      edges {
        node {
          title
          body {
            value
          }
        }
      }
    }
  }
`
